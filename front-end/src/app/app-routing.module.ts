import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from "./components/index/index.component";
import {LoginComponent} from "./components/login/login.component";
import {AuthGuard} from "./guards/authGuard";
import {RolesGuard} from "./guards/roles.guard";
import {UserGuard} from "./storage/guards/user.guard";
import {LoginGuard} from "./guards/login.guard";
import {HolidayGuard} from "./storage/guards/holiday.guard";

const routes: Routes = [{
  path: '',
  component: IndexComponent,
  canActivate: [AuthGuard, UserGuard, HolidayGuard]
}, {
  path: 'login',
  component: LoginComponent,
  canActivate: [LoginGuard]
}, {
  path: 'manage',
  loadChildren: () => import("./manage.module/manage.module").then(m => m.ManageModule),
  canActivate: [AuthGuard, RolesGuard, UserGuard, HolidayGuard]
},
  {
    path: '**',
    redirectTo: '/',
    canActivate: [AuthGuard, UserGuard, HolidayGuard]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
