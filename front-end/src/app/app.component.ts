import {Component} from '@angular/core';
import {AuthService} from "./services/authService";
import {StorageService} from "./services/storageService";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: Observable<string>;

  constructor(
    protected authService: AuthService,
    protected storageService: StorageService) {
    authService.isLoggedId$.next(!!authService.getJWT());
    this.title = storageService.getPageTitle();
  }
}
