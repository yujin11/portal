import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from "./modules/SharedModule";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {LoginService} from "./services/login.service";
import {AuthService} from "./services/authService";
import {AuthInterceptor} from "./services/authInterceptor";
import {components, entryComponents} from "./components";
import {StoreModule} from '@ngrx/store';
import * as fromStorage from './storage';
import {EffectsModule} from "@ngrx/effects";
import {UserEffect} from "./storage/effects/user.effect";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {VocationRequestEffect} from "./storage/effects/vocationRequest.effect";
import {UserListEffect} from "./storage/effects/user-list.effect";
import {HolidaysEffect} from "./storage/effects/holidays.effect";

@NgModule({
  declarations: [
    ...components,
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    StoreModule.forRoot({
      user: fromStorage.userReducer,
      vocation: fromStorage.vocationRequestReducer,
      userList: fromStorage.userListReducer,
      holidays: fromStorage.holidaysReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    }),
    EffectsModule.forRoot([UserEffect, VocationRequestEffect, UserListEffect, HolidaysEffect])
  ],
  entryComponents: [...entryComponents],
  providers: [LoginService, AuthService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
