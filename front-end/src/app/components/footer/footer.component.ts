import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
  selector: 'app-footer-component',
  templateUrl: './footer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {

}
