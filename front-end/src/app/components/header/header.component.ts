import {ChangeDetectionStrategy, Component} from "@angular/core";
import {UserModel} from "../../models/user.model";
import {Observable, of, timer} from "rxjs";
import {Router} from "@angular/router";
import {LoginService} from "../../services/login.service";
import {select, Store} from "@ngrx/store";
import * as fromStorage from '../../storage';
import {map, mergeMap} from "rxjs/operators";

@Component({
  selector: 'app-header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {

  user$: Observable<UserModel> = this.store.pipe(select(fromStorage.getUserSelector)).pipe(map((user: UserModel) => {
    if (!!user && user.UserRole.role != 'employee') {
      timer(1, 60000).pipe(mergeMap(() => {
        this.store.dispatch(fromStorage.loadNotViewedRequestsCount());
        return of(null);
      })).subscribe(t => t);
    }
    return user;
  }));
  isLoggedId$: Observable<boolean>;
  newRequests$ = this.store.select(fromStorage.getNotViewedRequestsCountSelector);

  constructor(
    protected router: Router,
    protected loginService: LoginService,
    protected store: Store<fromStorage.UserState>
  ) {
    this.isLoggedId$ = store.select(fromStorage.getUserIdSelector).pipe(mergeMap(user => of(!!user)));
  }

  logOut() {
    this.loginService.logOut();
    this.router.navigate(['/login']);
  }

}
