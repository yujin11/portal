import {IndexComponent} from './index/index.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './login/login.component';
import {SetDateComponent} from './set-date/set-date.component';
import {AddEditHolidayModal} from "../manage.module/components/add-edit-holiday.modal/addEditHoliday.modal";

export const components = [IndexComponent, FooterComponent, HeaderComponent, LoginComponent, SetDateComponent, AddEditHolidayModal];
export const entryComponents = [SetDateComponent, AddEditHolidayModal];
