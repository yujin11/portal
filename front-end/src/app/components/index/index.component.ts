import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from "@angular/core";
import {StorageService} from "../../services/storageService";
import {VocationModel} from "../../models/vocation.model";
import * as moment from 'moment';
import {VocationService} from "../../services/vocation.service";
import {MatDialog} from "@angular/material/dialog";
import {SetDateComponent} from "../set-date/set-date.component";
import {SetDateDialogModel} from "../../models/setDateDialog.model";
import * as fromStorage from '../../storage';
import {select, Store} from "@ngrx/store";
import {DateModel} from "../../models/user.date.model";
import {HolidaysState} from "../../storage";
import {HolidaysModel} from "../../models/holidays.model";
import {mergeMap, switchMap, tap} from "rxjs/operators";

@Component({
  selector: 'index-component',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexComponent {

  vocationList: VocationModel[] = [];
  holidaysList: HolidaysModel[] = [];
  isLoading = true;
  showData = false;
  currentDate = moment();
  monthNumber = this.currentDate.month();
  yearNumber = this.currentDate.year();
  period: DateModel[];
  userId = null;

  constructor(
    protected globalStorage: StorageService,
    protected vocationService: VocationService,
    protected cr: ChangeDetectorRef,
    protected dialog: MatDialog,
    protected store: Store<fromStorage.UserState>,
    protected holidaysStore: Store<fromStorage.HolidaysState>
  ) {
    this.store.pipe(select(fromStorage.getUserSelector)).subscribe(user => {
      if (user) {
        this.userId = user.id;
        this.getVocation();
      }
    });
  }


  setCurrentMonthNumber() {
    this.monthNumber = this.currentDate.month();
    this.yearNumber = this.currentDate.year();
  }

  onShowVocationDialog() {
    const data: SetDateDialogModel = {
      vocationList: this.vocationList,
      holidaysList: this.holidaysList,
      vocation: null,
      available: 7,
      payed: true
    };
    const dialogRef = this.dialog.open(SetDateComponent, {
      data: data
    });
    dialogRef.afterClosed().subscribe((data: SetDateDialogModel) => {
      if (!data)
        return;
      const vocationList = []
      for (let vocation of data.vocation) {
        vocation.payed = data.payed;
      }
      if (data && data.vocation && data.vocation.length > 0) {
        this.vocationService.addUserVocation(this.userId, data.vocation).subscribe(data => data);
      }
    });
  }

  setDate(monthNumber: number) {
    this.monthNumber = monthNumber;
    if (monthNumber < 0) {
      this.monthNumber = 11;
      this.yearNumber--;
    }
    if (monthNumber > 11) {
      this.monthNumber = 0;
      this.yearNumber++;
    }
    if (this.currentDate.year() <= this.yearNumber) {
      if (this.currentDate.month() >= this.monthNumber)
        this.getVocation();
    }
  }

  showNextButton(): boolean {
    if (this.currentDate.year() <= this.yearNumber) {
      if (this.currentDate.month() > this.monthNumber)
        return true;
      return false;
    }
    return false;
  }

  getVocation() {
    this.isLoading = true;
    const daysInMonth = moment().year(this.yearNumber).month(this.monthNumber).daysInMonth();
    const startDate = moment().year(this.yearNumber).month(this.monthNumber).date(1);
    const endDate = moment().year(this.yearNumber).month(this.monthNumber).date(daysInMonth);
    this.holidaysStore.select(fromStorage.getHolidaysSelector).pipe(tap(list => {
      this.holidaysList = list;
    }), switchMap(() => {
      return this.vocationService.findVocationPeriod(parseInt(this.userId), startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'))
    })).subscribe((vocationList: VocationModel[]) => {
      this.vocationList = vocationList;
      this.getPeriod();
      this.globalStorage.setPageTitle(this.period[0].date.format('MMMM'));
      this.isLoading = false;
      this.showData = true;
      this.cr.detectChanges();
    })
  }

  isInRange(date: moment.Moment): DateModel {
    let rangeDate = {date: date, vocation: null};
    if (this.vocationList.length == 0)
      return rangeDate;
    this.vocationList.forEach((vocation: VocationModel) => {
      const vDate = moment(vocation.date);
      if (vDate.isSame(date, 'date')) {
        rangeDate = {date: date, vocation: vocation};
        return;
      }
    });
    return rangeDate;
  }

  getPeriod() {
    this.period = [];
    let daysInMonth = moment().year(this.yearNumber).month(this.monthNumber).daysInMonth();
    while (daysInMonth) {
      const current = moment().year(this.yearNumber).month(this.monthNumber).date(daysInMonth);
      const data = this.isInRange(current);
      this.period.unshift(data);
      daysInMonth--;
    }
  }

}
