import {Component} from "@angular/core";
import {LoginService} from "../../services/login.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/authService";
import {loginResponseModel} from "./loginModels";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {StorageService} from "../../services/storageService";
import {Store} from "@ngrx/store";
import * as fromStorage from '../../storage';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public isLoading = false;
  public errorMessage = '';

  constructor(
    protected service: LoginService,
    protected authService: AuthService,
    protected router: Router,
    protected globalStorage: StorageService,
    protected store: Store<fromStorage.UserState>
  ) {
    authService.isLoggedId$.next(false);
    service.logOut();
  }

  loginForm = new FormGroup({
    login: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  login() {
    if (this.loginForm.invalid)
      return;
    this.errorMessage = '';
    this.isLoading = true;
    const loginData = this.loginForm.getRawValue();
    this.service.login(loginData.login, loginData.password).subscribe((data: loginResponseModel) => {
        if (data.token) {
          this.authService.setJWT(data.token);
          this.store.dispatch(fromStorage.userLoadedAction({user: data.user}));
          this.globalStorage.set('userRole', data.user.UserRole.role);
          localStorage.setItem('userId', data.user.id.toString());
          this.router.navigate(['/'])
        }
      },
      ((error: HttpErrorResponse) => {
        if (error.status == 400) {
          this.errorMessage = 'Incorrect login or password.'
        }
      })
    )
  }

}
