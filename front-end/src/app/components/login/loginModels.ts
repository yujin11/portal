import {UserModel} from "../../models/user.model";

export interface loginRequestModel {
  login: string,
  password: string
}

export interface loginResponseModel {
  token: string,
  user: UserModel
}
