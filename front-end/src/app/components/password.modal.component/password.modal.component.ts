import {Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-password-modal',
  templateUrl: './password.modal.component.html',
  styleUrls: ['./password.modal.component.scss']
})
export class PasswordModalComponent {
  constructor(
    public dialogRef: MatDialogRef<PasswordModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { password, isReset: boolean }
  ) {
  }

  closeDialog() {
    this.dialogRef.close('');
  }
}
