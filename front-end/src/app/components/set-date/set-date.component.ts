import {ChangeDetectionStrategy, Component, Inject} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SetDateDialogModel} from "../../models/setDateDialog.model";
import * as moment from 'moment';
import {DateRange} from 'moment-range';
import {VocationModel} from "../../models/vocation.model";
import {DateAdapter} from "@angular/material/core";
import {HolidaysModel} from "../../models/holidays.model";

@Component({
  selector: 'app-set-date',
  templateUrl: './set-date.component.html',
  styleUrls: ['./set-date.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SetDateComponent {

  public error = {
    hasErrors: false,
    message: ''
  };
  currentDate = moment().startOf('month');
  maxDate = this.currentDate.clone().add(1, 'month').startOf('month');
  startDate: moment.Moment;
  endDate: moment.Moment;

  constructor(
    public dialogRef: MatDialogRef<SetDateComponent>,
    private dateAdapter: DateAdapter<Date>,
    @Inject(MAT_DIALOG_DATA) public data: SetDateDialogModel
  ) {
    this.dateAdapter.setLocale('en');
    this.dateAdapter.getFirstDayOfWeek = () => {
      return 1;
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  checkDateRange(): boolean {
    if (!this.startDate || !this.endDate) {
      return true;
    }
    if (this.startDate.year() > this.endDate.year()) {
      this.error.hasErrors = true;
      this.error.message = 'End date should be grater then start date';
      return false;
    }
    if (this.startDate.month() > this.endDate.month()) {
      this.error.hasErrors = true;
      this.error.message = 'End date should be grater then start date';
      return false;
    }
    if (this.startDate.date() > this.endDate.date()) {
      this.error.hasErrors = true;
      this.error.message = 'End date should be grater then start date';
      return false;
    }
    const selectedDays = this.countSelectedDates(this.startDate, this.endDate);
    if (selectedDays > this.data.available) {
      this.error.hasErrors = true;
      this.error.message = `You have only ${this.data.available} available vocation days.`;
      return false;
    }
    this.error.hasErrors = false;
    this.data.vocation = [];
    const range = new DateRange(this.startDate, this.endDate);
    const arr = Array.from(range.by('days'));
    arr.forEach(date => {
      if (this.isValidDate(date)) {
        this.data.vocation.push({
          payed: this.data.payed,
          approved: false,
          date: date,
          viewed: false,
        });
      }
    });
    return true;
  }

  private countSelectedDates(startDate: moment.Moment, endDate: moment.Moment): number {
    const range = new DateRange(startDate, endDate);
    const arr = Array.from(range.by('days'));
    let finalDateList = arr.filter((date: moment.Moment) => {
      return this.isValidDate(date);
    });
    return finalDateList.length;
  }

  onDisableSubmit() {
    if (!this.startDate || !this.endDate)
      return true;
    if (this.startDate.year() > this.endDate.year())
      return true;
    if (this.startDate.month() > this.endDate.month())
      return true;
    if (this.startDate.date() > this.endDate.date())
      return true;
    return this.error.hasErrors;
  }

  isValidDate(date: moment.Moment) {
    if (date.month() < this.currentDate.month())
      return false;
    if (date.weekday() == 0 || date.weekday() == 6)
      return false;
    let isVocation = true;
    let isHoliday = true;
    if (this.data.vocationList.length > 0) {
      this.data.vocationList.forEach((vocation: VocationModel) => {
        const vDate = moment(vocation.date);
        if (vDate.isSame(date, 'date')) {
          isVocation = false;
          return;
        }
      });
    }
    if (this.data.holidaysList.length > 0) {
      this.data.holidaysList.forEach((holiday: HolidaysModel) => {
        const vDate = moment(holiday.date);
        if (vDate.isSame(date, 'date')) {
          isHoliday = false;
          return;
        }
      });
    }
    return (isVocation && isHoliday);
  }

}
