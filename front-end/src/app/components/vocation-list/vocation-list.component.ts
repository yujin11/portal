import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {VocationModel} from "../../models/vocation.model";
import {DateModel} from "../../models/user.date.model";
import * as moment from "moment";
import {HolidaysModel} from "../../models/holidays.model";

@Component({
  selector: 'app-vocation-list',
  templateUrl: './vocation-list.component.html',
  styleUrls: ['./vocation-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VocationListComponent {
  @Input() vocationList: VocationModel[];
  @Input() holidayList: HolidaysModel[];
  @Input() period: DateModel[];
  @Output() onDateClicked = new EventEmitter<moment.Moment>();

  isVocation(date: moment.Moment): HolidaysModel {
    let rangeDate = null;
    if (this.holidayList.length == 0)
      return rangeDate;
    this.holidayList.forEach((holiday: HolidaysModel) => {
      const vDate = moment(holiday.date);
      if (vDate.isSame(date, 'date')) {
        rangeDate = holiday;
        return;
      }
    });
    if (rangeDate)
      console.log('Vocation: ', rangeDate);
    return rangeDate;
  }

}
