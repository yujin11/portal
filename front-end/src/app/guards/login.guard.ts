import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromStorage from '../storage';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(
    protected userStore: Store<fromStorage.UserState>,
    protected userListStore: Store<fromStorage.UserState>,
    protected router: Router
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.userListStore.dispatch(fromStorage.userListLoadedAction({userList: []}));
    this.userStore.dispatch(fromStorage.userLoadedAction({user: null}));
    return true;
  }

}
