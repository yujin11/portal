import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {AuthService} from "../services/authService";
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class RolesGuard implements CanActivate {
  constructor(
    protected authService: AuthService,
    protected router: Router
  ) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const jwt = this.authService.getJWT();
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(jwt);
    const payload = helper.decodeToken(jwt);
    if (!!payload && !isExpired && payload.permissions && (payload.permissions.includes('manager') || payload.permissions.includes('admin')))
      return true
    this.router.navigate(['/']);
    return false;
  }

}
