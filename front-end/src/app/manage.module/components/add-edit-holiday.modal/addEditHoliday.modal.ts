import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ChangeDetectionStrategy, Component, Inject} from "@angular/core";
import {HolidaysModel} from "../../../models/holidays.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';
import {DateAdapter} from "@angular/material/core";

@Component({
  selector: 'app-add-edit-holiday',
  templateUrl: './addEditHoliday.modal.html',
  styleUrls: ['./addEditHoliday.modal.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddEditHolidayModal {
  isEdit = false;
  addEditForm = new FormGroup({
    date: new FormControl(moment(), [Validators.required]),
    title: new FormControl('', [Validators.required]),
  });

  constructor(
    public dialogRef: MatDialogRef<AddEditHolidayModal>,
    private dateAdapter: DateAdapter<Date>,
    @Inject(MAT_DIALOG_DATA) public data: HolidaysModel
  ) {
    this.dateAdapter.setLocale('en');
    this.dateAdapter.getFirstDayOfWeek = () => {
      return 1;
    }
    if (data) {
      this.isEdit = true;
      this.addEditForm.get('date').setValue(data.date);
      this.addEditForm.get('title').setValue(data.title);
    }
  }

  onSubmit() {
    const data = this.addEditForm.getRawValue();
    this.dialogRef.close(data);
  }

  closeDialog() {
    this.dialogRef.close(null);
  }

  isValidDate(date: moment.Moment) {
    if (date.weekday() == 0 || date.weekday() == 6)
      return false;
    return true;
  }
}
