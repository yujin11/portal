import {Component} from "@angular/core";
import {UserService} from "../../../services/userService";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import * as fromStorage from '../../../storage';
import {Store} from "@ngrx/store";
import {map, tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {UserModel} from "../../../models/user.model";
import {AddUserRequestModel} from "../../../models/addUserRequest.model";
import {MatDialog} from "@angular/material/dialog";
import {PasswordModalComponent} from "../../../components/password.modal.component/password.modal.component";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent {

  addUserForm: FormGroup;
  roleList = [
    {role: 'employee', label: 'Employee'},
    {role: 'manager', label: 'Manager'}
  ];
  canSelectRole = false;
  user$: Observable<UserModel>;
  checkingLogin = false;
  isLoading = false;

  constructor(
    protected userService: UserService,
    protected store: Store<fromStorage.UserState>,
    protected userListStore: Store<fromStorage.UserListState>,
    protected dialog: MatDialog,
  ) {
    this.addUserForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      login: new FormControl('', [Validators.required], this.loginDuplicateValidator.bind(this)),
      email: new FormControl('', [Validators.required, Validators.email]),
      userRole: new FormControl(this.roleList[0].role, [Validators.required])
    });
    this.user$ = store.select(fromStorage.getUserSelector).pipe(
      tap(
        user => {
          this.canSelectRole = (!!user && user.UserRole.role == 'admin');
        }
      )
    );
  }

  onAddUser() {
    if (this.addUserForm.invalid)
      return;
    this.addUser();
  }

  private addUser() {
    this.isLoading = true;
    const user: AddUserRequestModel = this.addUserForm.getRawValue();
    this.userService.addNew(user).subscribe(password => {
      this.dialog.open(PasswordModalComponent, {data: {password: password, isRest: false}});
      this.userListStore.dispatch(fromStorage.loadUserListAction());
      this.addUserForm.reset();
    }, (error => {
    }), () => {
      this.isLoading = false
    });
  }

  loginDuplicateValidator(control: AbstractControl) {
    this.checkingLogin = true;
    return this.userService.checkLogin(control.value).pipe(map(d => {
      this.checkingLogin = false;
      return d ? {duplicate: true} : null
    }));
  }

}
