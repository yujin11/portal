import {Component} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromStorage from '../../../storage';
import {faEdit, faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import {HolidaysModel} from "../../../models/holidays.model";
import {MatDialog} from "@angular/material/dialog";
import {AddEditHolidayModal} from "../add-edit-holiday.modal/addEditHoliday.modal";
import {HolidaysService} from "../../../services/holidaysService";

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.scss']
})
export class HolidaysComponent {
  constructor(
    protected store: Store<fromStorage.HolidaysState>,
    protected holidayService: HolidaysService,
    protected dialog: MatDialog) {
  }

  holidaysList$ = this.store.select(fromStorage.getHolidaysSelector);
  columnsToDisplay = ['date', 'title', 'edit', 'delete'];
  faTrashAlt = faTrashAlt;
  faEdit = faEdit;

  onAddHoliday() {
    this.dialog.open(AddEditHolidayModal, {width: '650px'}).afterClosed()
      .subscribe(data => {
        if (!data)
          return;
        this.holidayService.addHoliday(data).subscribe(data => this.store.dispatch(fromStorage.loadHolidaysAction()));
      });
  }

  onDeleteHoliday(holiday: HolidaysModel) {
    this.holidayService.deleteHoliday(holiday.id).subscribe(data => this.store.dispatch(fromStorage.loadHolidaysAction()));
  }

  onEditHoliday(holiday: HolidaysModel) {
    this.dialog.open(AddEditHolidayModal, {data: {date: holiday.date, title: holiday.title}, width: '650px'})
      .afterClosed()
      .subscribe((data: any) => {
        if (!data)
          return;
        holiday.title = data.title;
        holiday.date = data.date;
        this.holidayService.updateHoliday(holiday).subscribe(data => this.store.dispatch(fromStorage.loadHolidaysAction()));
      });
  }

}
