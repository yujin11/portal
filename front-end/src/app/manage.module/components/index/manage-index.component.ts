import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

@Component({
  selector: 'app-manage-component',
  templateUrl: './manage-index.component.html',
  styleUrls: ['./manage-index.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageIndexComponent implements OnInit {

  navLinks = [
    {
      path: './',
      label: 'Request',
    },
    {
      path: 'usersList',
      label: 'Users'
    },
    {
      path: 'addUser',
      label: 'Add User'
    },
    {
      path: 'holidays',
      label: 'Holidays'
    }
  ];

  constructor(private router: Router) {

  }

  ngOnInit(): void {
  }
}
