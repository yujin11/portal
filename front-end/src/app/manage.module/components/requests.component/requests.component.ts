import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {StorageService} from "../../../services/storageService";
import {VocationRequestsService} from "../../../services/vocationRequestsService";
import {Store} from "@ngrx/store";
import * as fromStorage from "../../../storage";
import {tap} from "rxjs/operators";
import {VocationRequestModel} from "../../models/vocationRequest.model";
import {Observable} from "rxjs";
import {faCheck, faTimes} from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-user-requests-component',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.scss']
})
export class RequestsComponent implements OnInit {
  requestsList$: Observable<VocationRequestModel[]>;
  columnsToDisplay = ['User', 'approved', 'createdAt', 'Vocation', 'approve', 'decline'];
  faCheck = faCheck;
  faTimes = faTimes;

  constructor(
    protected storageService: StorageService,
    protected vocationRequestService: VocationRequestsService,
    protected dc: ChangeDetectorRef,
    protected store: Store<fromStorage.RequestState>
  ) {
  }

  private getRequests() {
    this.requestsList$ = this.vocationRequestService.getNotApproved();
    this.dc.detectChanges();
  }

  acceptRequest(id) {
    this.vocationRequestService.approveRequest(id).pipe(tap(r => this.getRequests())).subscribe(r => r);
  }

  declineRequest(id) {
    this.vocationRequestService.declineRequest(id).pipe(tap(r => this.getRequests())).subscribe(r => r);
  }

  ngOnInit(): void {
    this.storageService.setPageTitle('Manage');
    this.requestsList$ = this.vocationRequestService.getNotApproved().pipe(tap((requests: VocationRequestModel[]) => {
      let notViewedRequest = [];
      requests.forEach((r) => {
        if (!r.viewed)
          notViewedRequest.push(r.id);
      });
      if (notViewedRequest.length > 0)
        this.vocationRequestService.setViewedRequest(notViewedRequest).subscribe(r => {
          this.store.dispatch(fromStorage.setNotViewedRequestsCount({count: 0}));
        });
    }));
  }
}
