import {Component} from "@angular/core";
import {UserService} from "../../../services/userService";
import {Observable} from "rxjs";
import {UserModel} from "../../../models/user.model";
import * as moment from "moment";
import {DateModel} from "../../../models/user.date.model";
import {VocationModel} from "../../../models/vocation.model";
import {faCog, faKey, faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import {Store} from "@ngrx/store";
import * as fromStorage from '../../../storage';
import {MatDialog} from "@angular/material/dialog";
import {PasswordModalComponent} from "../../../components/password.modal.component/password.modal.component";
import {switchMap, tap} from "rxjs/operators";
import {HolidaysModel} from "../../../models/holidays.model";

@Component({
  selector: 'app-manage-users-component',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  userList$: Observable<UserModel[]>;
  holidaysList: HolidaysModel[];
  currentDate = moment();
  monthNumber = this.currentDate.month();
  yearNumber = this.currentDate.year();
  faCog = faCog;
  faTrashAlt = faTrashAlt;
  faKey = faKey;

  constructor(
    protected userService: UserService,
    protected dialog: MatDialog,
    protected holidaysStore: Store<fromStorage.HolidaysState>,
    protected store: Store<fromStorage.UserListState>) {
    this.userList$ = this.holidaysStore.select(fromStorage.getHolidaysSelector).pipe(tap(list => {
      this.holidaysList = list;
    }), switchMap(() => {
      return this.store.select(fromStorage.getUserListSelector);
    }));
  }

  isInRange(date: moment.Moment, vocationList: VocationModel[]): DateModel {
    let rangeDate = {date: date, vocation: null};
    if (vocationList.length == 0)
      return rangeDate;
    vocationList.forEach((vocation: VocationModel) => {
      const vDate = moment(vocation.date);
      if (vDate.isSame(date, 'date')) {
        rangeDate = {date: date, vocation: vocation};
        return;
      }
    });
    return rangeDate;
  }

  getPeriod(vocationList: VocationModel[]) {
    let period: DateModel[] = [];
    let daysInMonth = moment().year(this.yearNumber).month(this.monthNumber).daysInMonth();
    while (daysInMonth) {
      const current = moment().year(this.yearNumber).month(this.monthNumber).date(daysInMonth);
      const data = this.isInRange(current, vocationList);
      period.unshift(data);
      daysInMonth--;
    }
    return period;
  }

  onUserDelete(userId: number) {
    this.userService.deleteUser(userId).subscribe(data => {
      this.store.dispatch(fromStorage.loadUserListAction());
    });
  }

  onResetUserPassword(userId: number) {
    this.userService.resetPassword(userId).subscribe(password => {
      this.dialog.open(PasswordModalComponent, {data: {password: password, isRest: true}});
    });
  }
}
