import {NgModule} from "@angular/core";
import {SharedModule} from "../modules/SharedModule";
import {ManageIndexComponent} from "./components/index/manage-index.component";
import {RouterModule} from "@angular/router";
import {routes} from "./routes";
import {UsersComponent} from "./components/usersList/users.component";
import {RequestsComponent} from "./components/requests.component/requests.component";
import {ManageService} from "./manage.service";
import {AddUserComponent} from "./components/add-user.component/add-user.component";
import {HolidaysComponent} from "./components/holidays.component/holidays.component";

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  providers: [ManageService],
  declarations: [ManageIndexComponent, UsersComponent, RequestsComponent, AddUserComponent, HolidaysComponent]
})
export class ManageModule {

}
