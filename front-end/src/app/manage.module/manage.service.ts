import {Injectable, isDevMode} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserModel} from "../models/user.model";

@Injectable()
export class ManageService {
  private url = (isDevMode()) ? 'http://localhost:8080/manage' : 'http://api.portal.com/manage';

  constructor(protected http: HttpClient) {

  }

  getUserList(): Observable<{ count: number, rows: UserModel[] }> {
    return this.http.get<{ count: number, rows: UserModel[] }>(this.url + '/vocations');
  }
}
