import {VocationModel} from "../../models/vocation.model";
import {UserModel} from "../../models/user.model";

export interface VocationRequestModel {
  id: number,
  Vocation: VocationModel[],
  User: UserModel,
  approved: boolean,
  viewed: boolean,
  approvedBy: number,

}
