import {Routes} from "@angular/router";
import {ManageIndexComponent} from "./components/index/manage-index.component";
import {RequestsComponent} from "./components/requests.component/requests.component";
import {UsersComponent} from "./components/usersList/users.component";
import {AddUserComponent} from "./components/add-user.component/add-user.component";
import {UserListGuard} from "../storage/guards/user-list.guard";
import {HolidaysComponent} from "./components/holidays.component/holidays.component";

export const routes: Routes = [
  {
    path: '',
    component: ManageIndexComponent,
    canActivate: [UserListGuard],
    children: [
      {
        path: '',
        component: RequestsComponent
      },
      {
        path: 'usersList',
        component: UsersComponent,
      },
      {
        path: 'addUser',
        component: AddUserComponent
      },
      {
        path: 'holidays',
        component: HolidaysComponent
      }
    ]
  }
];
