export interface AddUserRequestModel {
  login: string,
  name: string,
  lastName: string,
  email: string,
  role: string
}
