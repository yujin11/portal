import * as moment from "moment";

export interface HolidaysModel {
  date: moment.Moment,
  title: string,
  id: number
}
