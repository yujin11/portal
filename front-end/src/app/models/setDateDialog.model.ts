import {VocationModel} from "./vocation.model";
import {HolidaysModel} from "./holidays.model";

export interface SetDateDialogModel {
  vocationList: VocationModel[],
  vocation: VocationModel[],
  holidaysList: HolidaysModel[],
  available: number,
  payed: boolean
}
