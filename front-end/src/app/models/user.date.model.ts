import * as moment from "moment";
import {VocationModel} from "./vocation.model";
import {HolidaysModel} from "./holidays.model";

export interface DateModel {
  date: moment.Moment,
  vocation?: VocationModel,
  holiday?: HolidaysModel
}
