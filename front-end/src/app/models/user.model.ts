import {UserRolesModel} from "./userRoles.model";
import {VocationModel} from "./vocation.model";

export interface UserModel {
  id: number,
  name: string,
  lastName: string,
  login: string,
  UserRole?: UserRolesModel,
  Vocation?: [VocationModel]
}
