export interface UserRolesModel {
  id: number,
  role: string
}
