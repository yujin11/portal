import {Moment} from "moment";

export interface VocationModel {
  id?: number,
  date: Moment,
  payed: boolean,
  approved: boolean,
  viewed: boolean
}


