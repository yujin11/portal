import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatMenuModule} from "@angular/material/menu";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MomentModule} from "angular2-moment";
import {MatCardModule} from "@angular/material/card";
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatMomentDateModule} from "@angular/material-moment-adapter";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatBadgeModule} from "@angular/material/badge";
import {CommonModule} from "@angular/common";
import {MatTableModule} from "@angular/material/table";
import {MatTabsModule} from "@angular/material/tabs";
import {MatListModule} from "@angular/material/list";
import {VocationListComponent} from "../components/vocation-list/vocation-list.component";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatSelectModule} from "@angular/material/select";
import {PasswordModalComponent} from "../components/password.modal.component/password.modal.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {DatePickerComponent} from "../components/date-picker/date-picker.component";

const materialModuleList = [
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule,
  MatMenuModule,
  MatCardModule,
  MatDialogModule,
  MatDatepickerModule,
  MatMomentDateModule,
  MatTableModule,
  HttpClientModule,
  ReactiveFormsModule,
  FormsModule,
  MatBadgeModule,
  CommonModule,
  MatTabsModule,
  MatListModule,
  MatGridListModule,
  MatSelectModule
];

const imports = [...materialModuleList, FlexLayoutModule, MomentModule, FontAwesomeModule];
const entryComponents = [PasswordModalComponent, DatePickerComponent];

@NgModule({
  exports: [
    imports,
    VocationListComponent
  ],
  imports: [imports],
  entryComponents: entryComponents,
  declarations: [VocationListComponent, PasswordModalComponent, DatePickerComponent]
})
export class SharedModule {

}
