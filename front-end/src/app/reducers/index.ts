import { UserState} from '../storage/reducers/user.reducer';

export interface AppState {
  selectedUser: UserState
}
