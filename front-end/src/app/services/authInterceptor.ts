import {Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, of, throwError} from "rxjs";
import {AuthService} from "./authService";
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    protected authService: AuthService,
    protected router: Router
  ) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let jwt = this.authService.getJWT();
    if (jwt) {
      req = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${jwt}`)
      });
    }
    return next.handle(req).pipe(catchError((err: HttpErrorResponse) => {
      if (err && err.status == 401) {
        this.router.navigate(['/login']);
        return of(null)
      } else {
        return throwError(err);
      }
    }))
  }

}
