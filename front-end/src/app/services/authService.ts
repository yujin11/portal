import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable()
export class AuthService {

  public isLoggedId$ = new BehaviorSubject(false);

  setJWT(jwt): void {
    localStorage.setItem('jwt', jwt);
    this.isLoggedId$.next(true);
  }

  getJWT(): string {
    return localStorage.getItem('jwt');
  }
}
