import {Injectable, isDevMode} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {HolidaysModel} from "../models/holidays.model";
import * as moment from "moment";
import {Store} from "@ngrx/store";
import * as formStorage from '../storage';
import {map, share} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class HolidaysService {
  private url = (isDevMode()) ? 'http://localhost:8080/holidays' : 'http://api.portal.com/holidays';

  constructor(
    protected http: HttpClient,
    protected store: Store<formStorage.HolidaysState>) {

  }

  findAll(): Observable<HolidaysModel[]> {
    return this.http.get<HolidaysModel[]>(this.url);
  }

  findPeriod(from: moment.Moment, to: moment.Moment): Observable<HolidaysModel[]> {
    return this.store.select(formStorage.getHolidaysSelector).pipe(map(holidays => {
      return holidays.filter(h => {
        return moment(h.date).isBetween(from, to);
      });
    }), share());
  }

  addHoliday(holiday: HolidaysModel): Observable<HolidaysModel> {
    return this.http.post<HolidaysModel>(this.url,
      {date: holiday.date.format("YYYY-MM-DD"), title: holiday.title});
  }

  updateHoliday(holiday: HolidaysModel): Observable<HolidaysModel> {
    return this.http.put<HolidaysModel>(this.url + '/' + holiday.id,
      {date: holiday.date.format("YYYY-MM-DD"), title: holiday.title});
  }

  deleteHoliday(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
