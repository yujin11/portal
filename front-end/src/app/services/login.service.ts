import {Injectable, isDevMode} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {loginResponseModel} from "../components/login/loginModels";
import {StorageService} from "./storageService";

@Injectable()
export class LoginService {
  constructor(
    protected http: HttpClient,
    protected globalStorage: StorageService
  ) {

  }

  private url = (isDevMode()) ? 'http://localhost:8080/login' : 'http://api.portal.com/login';

  public login(name: string, password: string): Observable<loginResponseModel> {
    return this.http.post<loginResponseModel>(this.url, {login: name, password: password});
  }

  public logOut() {
    localStorage.clear();
    this.globalStorage.clearStorage();
  }
}
