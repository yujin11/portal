import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private storage = {};
  private pageTitle$ = new BehaviorSubject('');


  get(fieldName) {
    return this.storage[fieldName];
  }

  set(fieldName, data) {
    this.storage[fieldName] = data;
  }

  clearStorage() {
    this.storage = {};
  }

  getPageTitle() {
    return this.pageTitle$.asObservable();
  }

  setPageTitle(title: string) {
    this.pageTitle$.next(title);
  }


}
