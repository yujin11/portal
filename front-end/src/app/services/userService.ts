import {Injectable, isDevMode} from "@angular/core";
import {Observable} from "rxjs";
import {UserModel} from "../models/user.model";
import {HttpClient} from "@angular/common/http";
import {AddUserRequestModel} from "../models/addUserRequest.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(protected http: HttpClient) {

  }

  private url = (isDevMode()) ? 'http://localhost:8080/user' : 'http://api.portal.com/user';

  getById(id: string): Observable<UserModel> {
    return this.http.get<UserModel>(this.url + '/' + id);
  }

  getList(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.url);
  }

  getListWithVocations(): Observable<UserModel[]> {
    return this.http.get<UserModel[]>(this.url + '/vocations');
  }

  findVocationPeriod(id: number, startDate: string, endDate: string): Observable<UserModel> {
    return this.http.post<UserModel>(this.url + '/vocationPeriod', {
      userId: id,
      startDate: startDate,
      endDate: endDate
    })
  }

  checkLogin(login: string): Observable<boolean> {
    return this.http.get<boolean>(this.url + '/login/' + login);
  }

  addNew(user: AddUserRequestModel): Observable<string> {
    return this.http.post<{ user: string, password: string }>(this.url, user).pipe(map(d => d.password));
  }

  deleteUser(userId: number) {
    return this.http.delete(this.url + '/' + userId.toString());
  }

  resetPassword(userId: number): Observable<string> {
    return this.http.patch<string>(this.url + '/password', {id: userId});
  }

}
