import {Injectable, isDevMode} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {VocationModel} from "../models/vocation.model";
import * as moment from "moment";

@Injectable({
  providedIn: 'root'
})
export class VocationService {

  constructor(protected http: HttpClient) {

  }

  private url = (isDevMode()) ? 'http://localhost:8080/vocation' : 'http://api.portal.com/vocation';


  findVocationPeriod(id: number, startDate: string, endDate: string): Observable<VocationModel[]> {
    return this.http.post<VocationModel[]>(this.url, {
      userId: id,
      startDate: startDate,
      endDate: endDate
    })
  }

  addUserVocation(userId: number, vocation: VocationModel[]): Observable<any> {
    const dates = [];
    vocation.forEach((v: VocationModel) => {
      dates.push(v.date.format('YYYY-MM-DD'))
    });
    return this.http.put(this.url, {userId: userId, dates: dates, payed: vocation[0].payed});
  }

}
