import {Injectable, isDevMode} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {VocationRequestModel} from "../manage.module/models/vocationRequest.model";

@Injectable({
  providedIn: 'root'
})
export class VocationRequestsService {

  private url = (isDevMode()) ? 'http://localhost:8080/vocationRequest' : 'http://api.portal.com/vocationRequest';

  constructor(protected http: HttpClient) {

  }

  getUnViewedListCount() {
    return this.http.get<number>(this.url + '/notViewed');
  }

  getNotApproved() {
    return this.http.get<VocationRequestModel[]>(this.url + '/notApproved');
  }

  approveRequest(id) {
    return this.http.post<VocationRequestModel>(this.url + '/approve', {id: id});
  }

  declineRequest(id) {
    return this.http.post<VocationRequestModel>(this.url + '/decline', {id: id});
  }

  setViewedRequest(ids: number[]){
    return this.http.post<VocationRequestModel>(this.url + '/setViewed', {listIds: ids});
  }

}
