import {createAction, props} from "@ngrx/store";
import {HolidaysModel} from "../../models/holidays.model";

export const loadHolidaysAction = createAction('[Holidays] Load holidays action');
export const getHolidaysAction = createAction('[Holidays] Get holidays action');
export const loadedHolidaysAction = createAction('[Holidays] Loaded holidays action', props<{ list: HolidaysModel[] }>());
export const loadingHolidaysAction = createAction('[Holidays] Loading holidays action');
