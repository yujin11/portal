import {createAction, props} from "@ngrx/store";
import {UserModel} from "../../models/user.model";

export const loadUserListAction = createAction('[User List] Get User List');
export const getUserListAction = createAction('[User List] Get User List');
export const userListLoadedAction = createAction('[User List] User List Loaded', props<{ userList: UserModel[] }>());
export const userListLoadingAction = createAction('[User List] User List Loading');
export const userListLoadErrorAction = createAction('[User List] User List Error Loading', props<{ error: string }>());
