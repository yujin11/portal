import {createAction, props} from "@ngrx/store";
import {UserModel} from "../../models/user.model";

export const loadUserAction = createAction('[User] Get User', props<{ id: string }>());
export const getUserAction = createAction('[User] Get User');
export const userLoadedAction = createAction('[User] User Loaded', props<{ user: UserModel }>());
export const userLoadingAction = createAction('[User] User Loading');
export const userLoadErrorAction = createAction('[User] User Error Loading', props<{ error: string }>());
