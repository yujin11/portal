import {createAction, props} from "@ngrx/store";

export const getNotViewedRequestsCount = createAction('[Vocation Request] Get not viewed count');
export const loadNotViewedRequestsCount = createAction('[Vocation Request] Load not viewed count');
export const setNotViewedRequestsCount = createAction('[Vocation Request] Set not viewed count', props<{ count: number }>());
