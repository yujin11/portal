import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import * as fromActions from "../actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {HolidaysService} from "../../services/holidaysService";
import {HolidaysModel} from "../../models/holidays.model";

export class HolidaysEffect {
  constructor(
    private actions$: Actions<Action>,
    private holidaysService: HolidaysService
  ) {
  }

  loadUser$ = createEffect(() => this.actions$.pipe(
    ofType(fromActions.loadHolidaysAction),
    mergeMap((action) => this.holidaysService.findAll().pipe(
      map((list: HolidaysModel[]) => fromActions.loadedHolidaysAction({list: list})),
      catchError(() => EMPTY)
    ))
  ));
}
