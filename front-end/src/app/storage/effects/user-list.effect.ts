import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as fromActions from '../actions';
import {UserService} from "../../services/userService";
import {catchError, map, mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {Action} from "@ngrx/store";

@Injectable()
export class UserListEffect {
  constructor(
    private actions$: Actions<Action>,
    private userService: UserService
  ) {
  }

  loadUserList$ = createEffect(() => this.actions$.pipe(
    ofType(fromActions.loadUserListAction),
    mergeMap((action) => this.userService.getListWithVocations().pipe(
      map((data) => fromActions.userListLoadedAction({userList: data})),
      catchError(() => EMPTY)
    ))
  ));
}
