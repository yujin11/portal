import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import * as fromActions from '../actions';
import {UserService} from "../../services/userService";
import {catchError, map, mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {Action} from "@ngrx/store";
import {UserModel} from "../../models/user.model";

@Injectable()
export class UserEffect {
  constructor(
    private actions$: Actions<Action>,
    private userService: UserService
  ) {
  }

  loadUser$ = createEffect(() => this.actions$.pipe(
    ofType(fromActions.loadUserAction),
    mergeMap((action) => this.userService.getById(action.id).pipe(
      map((user: UserModel) => fromActions.userLoadedAction({user: user})),
      catchError(() => EMPTY)
    ))
  ));
}
