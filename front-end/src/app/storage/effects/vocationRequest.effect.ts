import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Injectable} from "@angular/core";
import {Action} from "@ngrx/store";
import {VocationRequestsService} from "../../services/vocationRequestsService";
import * as fromActions from "../actions";
import {catchError, map, mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";

@Injectable()
export class VocationRequestEffect {
  constructor(
    private actions$: Actions<Action>,
    protected vocationRequestService: VocationRequestsService,
  ) {
  }

  loadRequestNotViewedCount$ = createEffect(() => this.actions$.pipe(
    ofType(fromActions.loadNotViewedRequestsCount),
    mergeMap((action) => this.vocationRequestService.getUnViewedListCount().pipe(
      map((count: number) => fromActions.setNotViewedRequestsCount({count: count})),
      catchError(() => EMPTY)
    ))
  ));
}
