import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable, of} from "rxjs";
import {Store} from "@ngrx/store";
import * as fromStorage from '../index';
import {catchError, exhaustMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class HolidayGuard implements CanActivate {

  constructor(protected store: Store<fromStorage.HolidaysState>) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(fromStorage.getHolidaysLoadedSelector).pipe(exhaustMap(loaded => {
      if (!loaded) {
        this.store.dispatch(fromStorage.loadHolidaysAction());
      }
      return of(true);
    }), catchError(() => of(false)));
  }

}
