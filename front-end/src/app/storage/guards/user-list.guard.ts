import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromStorage from '../index';
import {catchError, exhaustMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserListGuard implements CanActivate {

  constructor(protected store: Store<fromStorage.UserListState>) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(fromStorage.getUserListSelector).pipe(exhaustMap(list => {
      if (list.length == 0) {
        this.store.dispatch(fromStorage.loadUserListAction());
      }
      return of(true);
    }), catchError(() => of(false)));
  }

}
