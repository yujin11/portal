import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromStorage from '../index';
import {catchError, exhaustMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(protected store: Store<fromStorage.UserState>) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.select(fromStorage.getUserSelector).pipe(exhaustMap(u => {
      if (u == null) {
        this.store.dispatch(fromStorage.loadUserAction({id: localStorage.getItem('userId')}));
      }
      return of(true);
    }), catchError(() => of(false)));
  }

}
