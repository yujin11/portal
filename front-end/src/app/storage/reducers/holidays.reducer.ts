import {HolidaysModel} from "../../models/holidays.model";
import {Action, createReducer, on} from "@ngrx/store";
import * as fromAction from '../actions';

export interface HolidaysState {
  holidays: HolidaysModel[],
  loaded: boolean,
  loading: boolean
}

export const initialState: HolidaysState = {
  holidays: [],
  loaded: false,
  loading: false
};

const reducer = createReducer(
  initialState,
  on(fromAction.getHolidaysAction, state => state),
  on(fromAction.loadHolidaysAction, state => state),
  on(fromAction.loadingHolidaysAction, state => ({...state, loading: true, loaded: false})),
  on(fromAction.loadedHolidaysAction, (state, {list}) => ({...state, holidays: list, loading: false, loaded: true}))
);

export function holidaysReducer(state: HolidaysState   | undefined, action: Action) {
  return reducer(state, action);
}
