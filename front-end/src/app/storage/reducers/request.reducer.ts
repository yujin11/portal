import {Action, createReducer, on} from "@ngrx/store";
import * as fromActions from '../actions';

export interface RequestState {
  requestsCount: number
};

export const requestInitialState: RequestState = {
  requestsCount: 0
};

const reducer = createReducer(requestInitialState,
  on(fromActions.getNotViewedRequestsCount, state => state),
  on(fromActions.loadNotViewedRequestsCount, state => state),
  on(fromActions.setNotViewedRequestsCount, (state, {count}) => ({...state, requestsCount: count}))
);

export function vocationRequestReducer(state: RequestState | undefined, action: Action) {
  return reducer(state, action);
}
