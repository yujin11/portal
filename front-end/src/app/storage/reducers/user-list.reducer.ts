import {Action, createReducer, on} from "@ngrx/store";
import * as fromAction from '../actions';
import {UserModel} from "../../models/user.model";

export interface UserListState {
  userList: UserModel[],
  loaded: boolean,
  loading: boolean,
  loadError: boolean,
  error: string
}

export const userListInitialState: UserListState = {
  userList: [],
  loaded: false,
  loading: false,
  loadError: false,
  error: ''
};

const reducer = createReducer(
  userListInitialState,
  on(fromAction.getUserListAction, state => state),
  on(fromAction.userListLoadingAction, state => ({...state, loading: true, loaded: false})),
  on(fromAction.userListLoadedAction, (state, {userList}) => ({
    ...state,
    userList: userList,
    loading: false,
    loaded: true,
    loadError: false,
    error: ''
  })),
  on(fromAction.userListLoadErrorAction, (state, {error}) => ({
    ...state,
    loading: false,
    loaded: false,
    loadError: true,
    error: error
  })),
);

export function userListReducer(state: UserListState | undefined, action: Action) {
  return reducer(state, action);
}
