import {Action, createReducer, on} from "@ngrx/store";
import * as fromAction from '../actions';
import {UserModel} from "../../models/user.model";

export interface UserState {
  user: UserModel,
  loaded: boolean,
  loading: boolean,
  loadError: boolean,
  error: string
}

export const userInitialState: UserState = {
  user: null,
  loaded: false,
  loading: false,
  loadError: false,
  error: ''
};

const reducer = createReducer(
  userInitialState,
  on(fromAction.getUserAction, state => state),
  on(fromAction.userLoadingAction, state => ({...state, loading: true, loaded: false})),
  on(fromAction.userLoadedAction, (state, {user}) => ({
    ...state,
    user: user,
    loading: false,
    loaded: true,
    loadError: false,
    error: ''
  })),
  on(fromAction.userLoadErrorAction, (state, {error}) => ({
    ...state,
    loading: false,
    loaded: false,
    loadError: true,
    error: error
  })),
);

export function userReducer(state: UserState | undefined, action: Action) {
  return reducer(state, action);
}
