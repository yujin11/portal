import {createFeatureSelector, createSelector} from "@ngrx/store";
import {HolidaysState} from '../reducers/holidays.reducer'

const stateSelector = createFeatureSelector<HolidaysState>('holidays');
export const getHolidaysStateSelector = createSelector(stateSelector, (state: HolidaysState) => state);
export const getHolidaysSelector = createSelector(stateSelector, (state: HolidaysState) => state.holidays);
export const getHolidaysLoadingSelector = createSelector(stateSelector, (state) => state.loading);
export const getHolidaysLoadedSelector = createSelector(stateSelector, (state) => state.loaded);
