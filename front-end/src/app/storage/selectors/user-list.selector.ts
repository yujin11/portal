import {createFeatureSelector, createSelector} from "@ngrx/store";
import {UserListState} from "../reducers/user-list.reducer";

const stateSelector = createFeatureSelector<UserListState>('userList');
export const getUserListSelector = createSelector(stateSelector, (state: UserListState) => state.userList);
export const getUserListLoadingSelector = createSelector(stateSelector, (state) => state.loading);
export const getUserListLoadedSelector = createSelector(stateSelector, (state) => state.loaded);
export const getUserListErrorSelector = createSelector(stateSelector, (state) => state.loadError);
export const getUserListErrorMessageSelector = createSelector(stateSelector, (state) => state.error);
