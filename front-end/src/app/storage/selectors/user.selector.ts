import {createFeatureSelector, createSelector} from "@ngrx/store";
import {UserState} from "../reducers/user.reducer";

const stateSelector = createFeatureSelector<UserState>('user');
export const getUserSelector = createSelector(stateSelector, (state: UserState) => state.user);
export const getUserIdSelector = createSelector(stateSelector, (state: UserState) => {
    return (state.user != null) ? state.user.id : null
  }
);
export const getUserLoadingSelector = createSelector(stateSelector, (state) => state.loading);
export const getUserLoadedSelector = createSelector(stateSelector, (state) => state.loaded);
export const getUserErrorSelector = createSelector(stateSelector, (state) => state.loadError);
export const getUserErrorMessageSelector = createSelector(stateSelector, (state) => state.error);
