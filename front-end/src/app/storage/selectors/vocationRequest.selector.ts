import {createFeatureSelector, createSelector} from "@ngrx/store";
import {RequestState} from '../reducers/request.reducer';

const stateSelector = createFeatureSelector<RequestState>('vocation');

export const getNotViewedRequestsCountSelector = createSelector(stateSelector, (state: RequestState) => state.requestsCount);
