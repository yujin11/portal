const express = require('express');
const cors = require("cors");
const indexRoutes = require('./src/routes');
const userRoutes = require('./src/routes/user');
const vocationRoutes = require('./src/routes/vocation');
const vocationRequestRoutes = require('./src/routes/vocationRequest');
const CryptoJS = require('crypto-js');
const ejwt = require('express-jwt');
const loginRoutes = require('./src/routes/login');
const holidayRoutes = require('./src/routes/holiday');
const session = require('express-session');
let guard = require('express-jwt-permissions')()


const JWT_SECRET = 'U2FsdGVkX181lBD0Zw8cpSYkPkBo5Hq1laLF1H6yWmg=';
const noAuthPaths = {
    path: [
        {
            url: '/login',
            methods: ['POST']
        }
    ]
};

const app = express();
app.use(express.json());
app.options("*", cors());
app.use(cors({
    origin: ['http://localhost:4200', 'http://localhost:8080', 'http://api.portal.com', 'http://portal.com'],
    credentials: true
}));
app.use(function (err, req, res, next) {
    if (err.code === 'permission_denied') {
        res.status(403).send('Forbidden');
    }
    next();
});
app.use(session({secret: JWT_SECRET}));
app.use(ejwt({
    secret: JWT_SECRET
}).unless(noAuthPaths));

app.use('/', indexRoutes);
app.use('/user', userRoutes);
app.use('/vocation', vocationRoutes);
app.use('/vocationRequest', guard.check([['admin'], ['manager']]), vocationRequestRoutes);
app.use('/login', loginRoutes);
app.use('/holidays', holidayRoutes);

app.listen(8080);

module.exports = {app};
