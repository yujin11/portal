const db = require("./index");

const Data = db.sequelize.define('data', {
    key: {
        type: db.Sequelizer.STRING,
        primaryKey: true
    },
    value: db.Sequelizer.STRING,
}, {
    timestamps: false,
});
module.exports = Data;
