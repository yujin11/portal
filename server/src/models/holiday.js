const db = require("./index");

const Holiday = db.sequelize.define('holiday', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    date: {
        type: db.Sequelizer.DATEONLY,
        allowNull: false
    },
    title: {
        type: db.Sequelizer.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
});

module.exports = Holiday;
