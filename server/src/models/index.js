let db = {};
const Sequelizer = require("sequelize");
const sequelize = new Sequelizer('portal', 'root', 'someRo0tP4ssword', {
    host: 'mysql',
    dialect: 'mariadb',
    charset: 'utf8',
    collate: 'utf8_general_ci',
    pool: {
        max: 5,
        min: 1,
        acquire: 30000,
        idle: 10000
    }
});
sequelize.sync();
db.sequelize = sequelize;
db.Sequelizer = Sequelizer;
module.exports = db;
