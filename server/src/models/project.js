const db = require("./index");

const Project = db.sequelize.define('project', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: db.Sequelizer.STRING,
    customer: db.Sequelizer.STRING
});

module.exports = Project;
