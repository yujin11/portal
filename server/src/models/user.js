const db = require("./index");
const UserRole = require("./userRoles");
const Project = require("./project");
const Vocation = require('./vocation');
const VocationRequest = require('./vocation.request');

const User = db.sequelize.define('user', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: db.Sequelizer.STRING,
    lastName: db.Sequelizer.STRING,
    login: db.Sequelizer.STRING,
    password: db.Sequelizer.STRING
});

User.belongsToMany(VocationRequest, {
    through: 'request_user',
    as: 'VocationRequest',
    timestamps: false
});
VocationRequest.belongsTo(User, {as: 'User', through: 'request_user', timestamps: false});
User.belongsToMany(Vocation, {through: 'vocation_user', as: 'Vocation', timestamps: false});
Vocation.belongsTo(User, {as: 'User', through: 'vocation_user', timestamps: false});
User.belongsTo(UserRole, {as: 'UserRole', through: 'user_role', timestamps: false});
UserRole.belongsToMany(User, {through: 'role_user', as: 'User', timestamps: false});
User.belongsToMany(Project, {through: 'user_projects', as: 'Projects', timestamps: false});
Project.belongsToMany(User, {through: 'project_users', as: 'Users', timestamps: false});

module.exports = User;
