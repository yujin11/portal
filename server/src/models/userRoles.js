const db = require("./index");

const UserRoles = db.sequelize.define('user_roles', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    role: db.Sequelizer.STRING,
}, {
    timestamps: false,
});
module.exports = UserRoles;
