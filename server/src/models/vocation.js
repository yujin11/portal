const db = require("./index");

const Vocation = db.sequelize.define('vocation', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    date: {
        type: db.Sequelizer.DATEONLY,
        allowNull: false
    },
    payed: {
        type: db.Sequelizer.BOOLEAN,
        defaultValue: false
    },
    approved: {
        type: db.Sequelizer.BOOLEAN,
        defaultValue: false
    }
}, {
    timestamps: false,
});
module.exports = Vocation;
