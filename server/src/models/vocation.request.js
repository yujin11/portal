const db = require("./index");
const Vocation = require('./vocation');

const VocationRequest = db.sequelize.define('vocation_request', {
    id: {
        type: db.Sequelizer.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    active: {
        type: db.Sequelizer.BOOLEAN,
        defaultValue: true
    },
    approved: {
        type: db.Sequelizer.BOOLEAN,
        defaultValue: false
    },
    viewed: {
        type: db.Sequelizer.BOOLEAN,
        defaultValue: false
    },
    approvedBy: {
        type: db.Sequelizer.INTEGER
    }
});
VocationRequest.belongsToMany(Vocation, {as: 'Vocation', through: 'request_vocation', timestamps: false});
Vocation.belongsTo(VocationRequest, {as: 'Request', through: 'request_vocation', constraints: false, timestamps: false});
module.exports = VocationRequest;
