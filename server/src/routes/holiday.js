const express = require('express');
const route = express.Router();
const holidayService = require('../services/holidaysService');

route.get('/', async (req, resp) => {
    try {
        const result = await holidayService.findAll();
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.get('/period', async (req, resp) => {
    try {
        const {startDate, endDate} = req.query;
        const result = await holidayService.findPeriod(startDate, endDate);
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.post('/', async (req, resp) => {
    try {
        const {date, title} = req.body;
        const result = await holidayService.addHoliday(date, title);
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.delete('/:id', async (req, resp) => {
    try {
        const {id} = req.params;
        const result = await holidayService.deleteHoliday(id);
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.put('/:id', async (req, resp) => {
    try {
        const {id} = req.params;
        const {date, title} = req.body;
        const holiday = await holidayService.findById(id);
        await holiday.update({
            date: date,
            title: title
        });
        return resp.json(holiday);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

module.exports = route;
