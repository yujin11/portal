const express = require("express");
const router = express.Router();

router.get('/', async (req, resp) => {
    resp.json({message: 'Welcome to home page'});
});

module.exports = router;
