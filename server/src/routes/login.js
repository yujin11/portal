const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const UserService = require('../services/userService');
const JWT_SECRET = 'U2FsdGVkX181lBD0Zw8cpSYkPkBo5Hq1laLF1H6yWmg=';
const token = require('../services/token');

router.post('/', async (req, resp) => {
    const data = req.body;
    try {
        const user = await UserService.findForLogin(data.login);
        if (!user) {
            throw new Error('Invalid login or password.');
        }
        bcrypt.compare(data.password, user.password, (err, res) => {
            if (err)
                throw new Error('Invalid login or password.');
            const JWT = token.create({
                payload: {
                    userId: user.id,
                    permissions: [user.UserRole.role],
                },
                secret: JWT_SECRET,
            });
            req.session.userRole = user.UserRole.role;
            resp.json({token: JWT, user: user});
        });
    } catch (e) {
        resp.send(400, e);
    }
});

module.exports = router;
