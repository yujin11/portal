const express = require('express');
const route = express.Router();
const UserService = require('../services/userService');
const User = require('../models/user');
const UserRole = require('../models/userRoles');
const Vocation = require('../models/vocation');
const token = require('../services/token');
let guard = require('express-jwt-permissions')();
const moment = require('moment');
const db = require('../models');

route.get('/vocations', guard.check([['admin'], ['manager']]), async (req, resp) => {
    try {
        const currentDate = moment();
        const monthNumber = currentDate.month();
        const yearNumber = currentDate.year();
        const daysInMonth = moment().year(yearNumber).month(monthNumber).daysInMonth();
        const startDate = moment().year(yearNumber).month(monthNumber).date(1);
        const endDate = moment().year(yearNumber).month(monthNumber).date(daysInMonth);
        const permission = await token.getPayload(req.headers.authorization).permissions[0];
        let whereQuery = (permission == 'admin') ? {role: {[db.Sequelizer.Op.not]: 'admin'}} : {role: 'employee'};
        const data = await User.findAll({
            include: [{
                model: UserRole,
                as: 'UserRole',
                attributes: ['role'],
                where: whereQuery
            }, {
                model: Vocation,
                as: 'Vocation',
                where: {
                    approved: true,
                    date: {
                        [db.Sequelizer.Op.between]: [startDate, endDate],
                    },
                },
                required: false
            }],
            attributes: {
                exclude: ['password', 'UserRoleId']
            }
        });
        resp.json(data);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.get('/login/:login', async (req, resp) => {
    try {
        const duplicate = await User.findAll({
            where: {
                login: req.params.login
            }
        });
        return resp.json(duplicate.length > 0);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.get('/:id', async (req, resp) => {
    try {
        const payload = await token.getPayload(req.headers.authorization);
        if (payload.userId != req.params.id) {
            if (payload.permissions[0] == 'employee' || payload.permissions[0] == 'manager')
                throw new Error('Forbidden');
        }
        const data = await User.findByPk(req.params.id, {
            include: [{
                model: UserRole,
                as: 'UserRole',
                attributes: ['role']
            }],
            attributes: {
                exclude: ['password', 'UserRoleId']
            }
        });
        resp.json(data);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.get('/', guard.check([['admin'], ['manager']]), async (req, resp) => {
    try {
        const data = await User.findAll({
            include: [{
                model: UserRole,
                as: 'UserRole',
                attributes: ['role'],
                where: {
                    role: 'employee'
                },
            }],
            attributes: {
                exclude: ['password', 'UserRoleId']
            }
        });
        resp.json(data);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.post('/', guard.check([['admin'], ['manager']]), async (req, resp) => {
    const data = req.body;
    try {
        const permission = await token.getPayload(req.headers.authorization).permissions[0];
        if (data.userRole == 'admin')
            throw new Error("Can't add user with role: " + data.userRole);
        if (data.userRole == permission)
            throw new Error("Can't add user with role: " + data.userRole);
        const userPassword = await UserService.addUser(data);
        resp.json({user: data.name, password: userPassword});
    } catch (e) {
        resp.status(400).send(e.message);
    }
});

route.patch('/password', guard.check([['admin'], ['manager']]), async (req, resp) => {
    try {
        const {id} = req.body;
        const permission = await token.getPayload(req.headers.authorization).permissions[0];
        const user = await User.findByPk(id, {
            include: [{
                model: UserRole,
                as: 'UserRole',
                attributes: ['role'],
            }],
        });
        if (user.UserRole.role == 'admin')
            throw new Error('You can`t manage this user.');
        if (user.UserRole.role == permission)
            throw new Error('You can`t manage this user.');
        const updatedPassword = await UserService.resetPassword(id);
        await user.update({password: updatedPassword.resetPassword});
        return resp.json(updatedPassword.password);
    } catch (e) {
        resp.status(400).send(e.message);
    }
});

route.delete('/:id', guard.check([['admin'], ['manager']]), async (req, resp) => {
    try {
        const {id} = req.params;
        await User.destroy({where: {id: id}});
        return resp.status(200).send();
    } catch (e) {
        resp.status(400).send(e.message);
    }
});

module.exports = route;
