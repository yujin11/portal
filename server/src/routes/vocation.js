const express = require('express');
const route = express.Router();
const UserService = require('../services/userService');
const VocationService = require('../services/vocationService');
const VocationRequestService = require('../services/vocationRequestService');
const token = require('../services/token');
const db = require('../models/index');

route.post('/', async (req, resp) => {
    const {userId, startDate, endDate} = req.body;
    try {
        if (!userId || !startDate || !endDate) {
            throw 'Invalid request data';
        }
        const payload = await token.getPayload(req.headers.authorization);
        const user = await UserService.findById(userId);
        if (payload.userId != userId && payload.permissions[0] != 'admin') {
            if (payload.permissions[0] == 'employee')
                throw "This employee does not has permissions for this operation.";
            if (user.UserRole.role == 'manager')
                throw "The manager does not has permissions for this operation.";
        }
        const vocation = await VocationService.findVocationPeriod(userId, startDate, endDate, true);
        return resp.json(vocation);
    } catch (e) {
        return resp.status(400).json(e);
    }
});

route.post('/approve', async (req, resp) => {
    const {id, approved} = req.body;
    try {
        if (!id || approved == undefined)
            throw 'Invalid request params';
        const payload = await token.getPayload(req.headers.authorization);
        const request = await VocationRequestService.findOne(id);
        if (!request) {
            throw "Can't find request.";
        }
        if (approved) {
            await VocationRequestService.approveRequest(id);
        } else {
            await VocationRequestService.declineRequest(id);
        }
        return resp.json({message: `Request has been successfully ${(approved) ? 'approved' : 'deleted'}`});
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.post('/active', async (req, resp) => {
    const {userId, dates} = req.body;
    try {
        if (!userId || !dates) {
            throw Error('Invalid request data');
        }
        const activeVocation = await VocationService.findUserVocation(userId, dates, true);
        if (!activeVocation)
            throw new Error('This user has approved vocation in this period.');
        return resp.json(activeVocation);
    } catch (e) {
        return resp.status(400).json(e);
    }
});

route.put('/', async (req, resp) => {
    const {userId, dates, payed} = req.body;
    const transaction = db.sequelize.transaction(async (t) => {
        try {
            if (!userId || !dates || payed == undefined) {
                throw 'Invalid request data';
            }
            const payload = await token.getPayload(req.headers.authorization);
            const user = await UserService.findByIdFull(userId);
            if (payload.userId != userId && payload.permissions[0] != 'admin') {
                if (payload.permissions[0] == 'employee')
                    throw "This employee does not has permissions for this operation.";
                if (user.UserRole.role == 'manager')
                    throw "The manager does not has permissions for this operation.";
            }
            const vocationList = await VocationService.addVocation(user.id, dates, payed);
            const isEmployee = (payload.permissions[0] != 'admin' && payload.permissions[0] != 'manager');
            let vocationRequest = null
            if (isEmployee)
                vocationRequest = await VocationRequestService.add(user, vocationList);
            for (let vocation of vocationList) {
                await vocation.setUser(user);
                if (!isEmployee)
                    await vocation.set('approved', true);
                await vocation.setRequest(vocationRequest);
                await vocation.update();
            }
            const updatedUser = await UserService.addVocation(user, vocationList);
            if (!vocationList || (isEmployee && !vocationRequest) || !updatedUser)
                throw 'Error when adding vocations to user';
            return resp.json(vocationList);

        } catch (e) {
            return resp.status(400).json(e);
        }
    });
});

module.exports = route;
