const express = require('express');
const route = express.Router();
const VocationRequestService = require('../services/vocationRequestService');
const token = require('../services/token');

route.get('/', async (req, resp) => {
    try {
        return resp.json(await VocationRequestService.findAll());
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.post('/approve', async (req, resp) => {
    try {
        const id = req.body.id;
        const payload = await token.getPayload(req.headers.authorization);
        return resp.json(await VocationRequestService.approveRequest(id, payload.userId));
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.post('/decline', async (req, resp) => {
    try {
        const id = req.body.id;
        return resp.json(await VocationRequestService.declineRequest(id));
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.get('/approved', async (req, resp) => {
    try {
        return resp.json(await VocationRequestService.getApprovedList());
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

route.get('/notApproved', async (req, resp) => {
    try {
        const result = await VocationRequestService.getNotApprovedList();
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.get('/notViewed', async (req, resp) => {
    try {
        const result = await VocationRequestService.getUnViewedListCount();
        return resp.json(result);
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.post('/setViewed', async (req, resp) => {
    const {listIds} = req.body;
    try {
        return resp.json(await VocationRequestService.setViewed(listIds));
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});
route.get('/:id', async (req, resp) => {
    try {
        return resp.json(await VocationRequestService.findOne(req.params.id));
    } catch (e) {
        return resp.status(400).send(e.message);
    }
});

module.exports = route;

