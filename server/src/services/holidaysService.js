const Holiday = require('../models/holiday');
const db = require('../models');

class HolidaysService {
    async addHoliday(date, title) {
        return await Holiday.create({
            date: date,
            title: title
        });
    }

    async deleteHoliday(id) {
        return await Holiday.destroy({
            where: {
                id: id
            }
        });
    }

    async findById(id) {
        return await Holiday.findByPk(id);
    }

    async findAll() {
        return await Holiday.findAll();
    }

    async editHoliday(holiday) {
        return await holiday.update();
    }

    async findPeriod(startDate, endDate) {
        const list = await Holiday.findAll({
            where: {
                date: {
                    [db.Sequelizer.Op.between]: [startDate, endDate],
                },
            },
        });
        return list;
    }
}

module.exports = new HolidaysService();
