const jwt = require('jsonwebtoken');

function create({payload, secret, expiresIn = '1d'}) {
    return jwt.sign(payload, secret, {expiresIn});
}

function verify({token, secret, ignoreExpiration = false}) {
    return jwt.verify(token, secret, {ignoreExpiration});
}

function getPayload(header) {
    const token = header.split('Bearer ')[1];
    return jwt.decode(token);
}

module.exports = {
    create,
    verify,
    getPayload,
    decode: jwt.decode,
};
