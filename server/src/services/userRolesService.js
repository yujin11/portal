const UserRoles = require('../models/userRoles.js');

class UserRolesService {
    async getList() {
        return await UserRoles.findAll().map(role => role.role);
    }
}

module.exports = new UserRolesService();
