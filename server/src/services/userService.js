const User = require("../models/user");
const db = require('../models');
const Vocation = require('../models/vocation');
const Data = require('../models/data');
const UserRole = require('../models/userRoles');
const bcrypt = require('bcrypt');

function ransomPassword(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz?!@#$%^&*0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

class UserService {
    getList() {
        return User.findAndCountAll();
    }

    findById(id) {
        return User.findByPk(id, {
            include: ['UserRole']
        });
    }

    findByIdWithRequests(id) {
        return User.findByPk(id, {
            include: ['VocationRequest']
        });
    }

    findByIdWithVocations(id) {
        return User.findByPk(id, {
            include: ['Vocation']
        });
    }

    findByIdFull(id) {
        return User.findByPk(id, {
            include: ['Vocation', 'VocationRequest', 'UserRole']
        });
    }

    findVocationPeriod(id, startDate, endDate) {
        return User.findByPk(id, {
            include: ['UserRole', {
                model: Vocation,
                as: 'Vocation',
                where: {
                    approved: true,
                    [db.Sequelizer.Op.or]: [
                        {
                            startDate: {
                                [db.Sequelizer.Op.between]: [startDate, endDate],
                            },
                        },
                        {
                            endDate: {
                                [db.Sequelizer.Op.between]: [startDate, endDate]
                            }
                        }
                    ]
                }
            }],
        }).pipe(map(user => {
            if (isApproved && !!user)
                return user.Vocation.filter(vocation => vocation.approved);
            return user;
        }))
    }

    findByName(name, lastName) {
        return User.findOne({
            where: {
                name: name,
                lastName: lastName
            }
        });
    }

    findForLogin(login) {
        return User.findOne({
            where: {
                login: login
            },
            include: ['UserRole']
        });
    }

    async addUser(data) {
        let user = await this.findForLogin(data.login);
        if (user)
            throw 'User with this login already exist';
        const userRole = await UserRole.findOne({
            where: {
                role: data.userRole
            }, include: ['User']
        });
        if (!userRole)
            throw 'User role does not exist.';
        const SALT = await Data.findByPk('ENCRYPT_SALT');
        const password = ransomPassword(6);
        const userData = {
            name: data.name,
            lastName: data.lastName,
            login: data.login,
            password: await bcrypt.hash(password, 8)
        };
        const userModel = await User.create(userData);
        await userModel.setUserRole(userRole);
        await userModel.update();
        let users = await userRole.getUser();
        users.push(userModel);
        await userRole.setUser(users);
        await userRole.update();
        return password;
    }

    async addVocation(user, vocationList) {
        let list = await user.getVocation();
        list = list.concat(vocationList);
        user.setVocation(list);
        await user.update();
        return await user;
    }

    async resetPassword(id) {
        const SALT = await Data.findByPk('ENCRYPT_SALT');
        const password = ransomPassword(6);
        const resetPassword = await bcrypt.hash(password, 8);
        console.log('=========== password: ', password);
        console.log('=========== resetPassword: ', resetPassword);
        return {password: password, resetPassword: resetPassword};
    }
}

module.exports = new UserService();
