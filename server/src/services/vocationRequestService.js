const VocationRequest = require('../models/vocation.request');
const User = require('../models/user');
const Vocation = require('../models/vocation');
const db = require('../models');

class VocationRequestService {

    findAll() {
        return VocationRequest.findAll({
            include: [
                {
                    model: User,
                    as: 'User',
                    attributes: ['id', 'name', 'lastName']
                },
                {
                    model: Vocation,
                    as: 'Vocation',
                    attributes: ['date', 'approved']
                }
            ]
        });
    }

    getApprovedList() {
        return VocationRequest.findAll({
            where: {
                approved: true
            },
            include: [
                {
                    model: User,
                    as: 'User',
                    attributes: ['id', 'name', 'lastName']
                },
                {
                    model: Vocation,
                    as: 'Vocation',
                    attributes: ['date', 'approved']
                }
            ]
        });
    }

    getNotApprovedList() {
        return VocationRequest.findAll({
            where: {
                approved: false
            },
            include: [
                {
                    model: User,
                    as: 'User',
                    attributes: ['id', 'name', 'lastName']
                },
                {
                    model: Vocation,
                    as: 'Vocation',
                    attributes: ['date', 'approved']
                }
            ]
        });
    }

    async add(user, vacationList) {
        if (!user)
            throw 'Invalid data.';
        let request = await VocationRequest.create({});
        let userVocationRequests = await user.getVocationRequest();
        userVocationRequests.push(request);
        await user.setVocationRequest(userVocationRequests);
        await user.update();
        request = await this.findOne(request.id);
        await request.setUser(user);
        await request.setVocation(vacationList);
        await request.update();
        return request;
    }

    findOne(id) {
        return VocationRequest.findByPk(id, {
            include: [
                {
                    model: User,
                    as: 'User',
                    attributes: ['id', 'name', 'lastName']
                },
                {
                    model: Vocation,
                    as: 'Vocation',
                    attributes: ['date', 'approved', 'id']
                }
            ]
        });
    }

    async setViewed(idList) {
        try {
            const transaction = await db.sequelize.transaction();
            for (let id of idList) {
                const request = await VocationRequest.findByPk(id);
                await request.update({viewed: true}, {transaction: transaction});
            }
            await transaction.commit();
            return idList;
        } catch (e) {
            throw e;
        }
    }

    getUnViewedListCount() {
        return VocationRequest.count({
            where: {
                viewed: false
            }
        });
    }

    getUnViewedList() {
        return VocationRequest.findAll({
            include: [
                {
                    model: Vocation,
                    as: 'Vocation',
                    attributes: ['date', 'approved']
                },
                {
                    model: User,
                    as: 'User',
                    attributes: ['id', 'name', 'lastName']
                }
            ],
            where: {
                viewed: false
            }
        });
    }

    async approveRequest(id, userId) {
        const request = await this.findOne(id);
        try {
            if (!request)
                throw "Can't find request with this id";
            const transaction = await db.sequelize.transaction();
            const vocationList = await request.getVocation();
            for (let vocation of vocationList) {
                await vocation.update({approved: true}, {transaction: transaction})
            }
            await request.update({approved: true, approvedBy: userId}, {transaction: transaction});
            await transaction.commit();
            return request;
        } catch (e) {
            throw e;
        }
    }

    async declineRequest(id) {
        const request = await this.findOne(id);
        try {
            if (!request)
                throw "Can't find request with this id";
            const transaction = await db.sequelize.transaction();
            const vocationList = await request.getVocation();
            for (let vocation of vocationList) {
                await vocation.destroy({transaction: transaction})
            }
            await request.destroy({transaction: transaction});
            await transaction.commit();
            return request;
        } catch (e) {
            throw e;
        }
    }

}

module.exports = new VocationRequestService();
