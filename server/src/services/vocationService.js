const db = require('../models');
const Vocation = require('../models/vocation');
const User = require('../models/user');
const moment = require('moment');

class VocationService {

    async findVocationPeriod(userId, startDate, endDate, isApproved = false) {
        return Vocation.findAll({
            include: [{
                model: User,
                as: 'User',
                attributes: ['id', 'login', 'name'],
                where: {
                    id: userId
                }
            }],
            attributes: {
                exclude: ['RequestId', 'UserId']
            },
            where: {
                approved: isApproved,
                date: {
                    [db.Sequelizer.Op.between]: [startDate, endDate],
                },
            },
        });
    }

    async findUserVocation(userId, dates, isApproved = false) {
        return Vocation.findAll({
            include: [{
                model: User,
                as: 'User',
                attributes: ['id', 'login', 'name'],
                where: {
                    id: userId
                }
            }],
            attributes: {
                exclude: ['vocation_id', 'user_id']
            },
            where: {
                approved: isApproved,
                [db.Sequelizer.Op.or]: [
                    {date: dates},
                    {
                        date: {
                            [db.Sequelizer.Op.in]: [dates]
                        }
                    }
                ]
            },
        });
    }

    async addVocation(userId, dates, payed) {
        if (!userId || !dates || payed == undefined)
            throw 'Add vocation - invalid data.';
        let datesList = [];
        const list = await this.findUserVocation(userId, dates, true);
        if (list.length > 0) {
            throw "User already has approved vocation in this period.";
        }
        for (const date of dates) {
            const d = moment(date);
            if (d.day() == 6 || d.day() == 0) {
                throw "Weekend can't be requested as vocation.";
            }
            const vocation = await Vocation.create({
                date: new Date(date),
                payed: payed,
                approved: false,
                viewed: false,
            });
            datesList.push(vocation);
        }
        return datesList;
    }

    async deleteVocation(id) {
        return await Vocation.findByPk(id).destroy();
    }
}

module.exports = new VocationService();
